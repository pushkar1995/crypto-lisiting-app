import React, {useState, useEffect} from 'react';
import axios from 'axios'
import './App.css';
import Currency from './currency/Currency'

function App() {
  const [currencies, setCurrencies] = useState([])
  const [search, setSearch] = useState('')

  useEffect(() => {
    axios.get('https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false')
    .then(res => {
      setCurrencies(res.data)
      // console.log(res.data)
    })
    .catch(error => console.log(error))
  }, []);

  const handleChange = e => {
    setSearch(e.target.value)
  }

  const filteredCurrencies = currencies.filter(currency => 
    currency.name.toLowerCase().includes(search.toLowerCase())
    )

  return (
    <div className='currency-main-container'>
      <div className='currency-search'>
        <h1 className='currency-text'>Search a currency</h1>
        <form>
          <input 
            type="text" 
            placeholder="Search" 
            className="currency-input" 
            onChange={handleChange}
            />
        </form>
      </div>
      {filteredCurrencies.map(currency => {
        return (
          <Currency
            key={currency.id} 
            name={currency.name}
            type={currency.symbol}
            image={currency.image}
            marketcap={currency.market_cap}
            price={currency.current_price}
            priceChange={currency.price_change_percentage_24h}
            volume={currency.total_volume}
          />
        )
      })}
    </div>
  );
}

export default App;


// https://api.zabo.com/v1

// https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=100&page=1&sparkline=false