import React from 'react'
import './currency.css'

const Currency = ({ name,image,type,price,volume,priceChange,marketcap }) => {
    return (
        <div className='currency-container'>
           <div className='currency-row'>
             <div className='currency'>
                <h1>{name}</h1>
                <p className='currency-type'>{type}</p>
                <img src={image} alt='crypto' />
             </div>
             <div className='currency-data'>
                <p className='currency-price'>${price}</p>
                <p className='currency-volume'>${volume.toLocaleString()}</p>
                {priceChange < 0 ? (
                    <p className='currency-percent red'>{priceChange.toFixed(2)}%</p>
                ) : ( <p className='currency-percent green'>{priceChange.toFixed(2)}%</p>
                )}
                <p className='currency-marketcap'>
                   Mkt Cap: ${marketcap.toLocaleString()} 
                </p>
             </div>   
           </div> 
        </div>
    )
}

export default Currency
